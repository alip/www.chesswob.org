[[!meta title="ICCD 21st EU Team Championship"]]
[[!meta author="Lithuanian Chess Federation"]]
[[!meta date="2013.06.19 11:12:32"]]

[[!tag BlogPost]]
[[!tag Lichess]]
[[!tag PGN]]
[[!tag 2013EUTeamICCD]]

# <a href="/tourn#iccd-21-eu-team-champ">ICCD 21st EU Team Championship</a>
<hr style="width: 100%; color: black; height: 1px; background-color:black;" />

<div class="embed-responsive embed-responsive-1by1">
<iframe
style="height: 100%; width: 100%"
class="embed-responsive-item" src="https://lichess.org/study/embed/wY2hBtAO/5a8WygkE?bg=light&theme=green" allowfullscreen>
Your browser does not support iframes, use: https://lichess.org/study/wY2hBtAO/5a8WygkE
</iframe>
</div>

<br/>

<ul class="list-group">
<li class="list-group-item">
<ul class="list-inline">
<li class="list-inline-item"><a href="/pgn/iccd-eu-team-21.pgn">All Games</a></li>
<li class="list-inline-item"><a href="/pgn/iccd-eu-team-21.pgn">Bütün Oyunlar</a></li>
</ul>
</li>
<li class="list-group-item">
<ul class="list-inline">
<li class="list-inline-item"><a href="/pgn/iccd-eu-team-21_1.pgn">1.pgn</a></li>
<li class="list-inline-item"><a href="/pgn/iccd-eu-team-21_2.pgn">2.pgn</a></li>
<li class="list-inline-item"><a href="/pgn/iccd-eu-team-21_3.pgn">3.pgn</a></li>
<li class="list-inline-item"><a href="/pgn/iccd-eu-team-21_4.pgn">4.pgn</a></li>
<li class="list-inline-item"><a href="/pgn/iccd-eu-team-21_5.pgn">5.pgn</a></li>
</ul>
</li>
</ul>

<br/>
