#!/bin/sh
# Copyright 2020 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

root=$(git rev-parse --show-toplevel || echo /var/empty)/img
if [ -z "$1" ]; then
    find $root -iname '*.png' -exec optipng -o3 '{}' \+ 2>&1 | tee -a "$root"/optipng.log
else
    exec optipng -log "$root"/optipng.log -o3 "$@"
fi
